import ../logic
import unittest, math, strformat

const
  NORMAL_TEST_FP = "tests/test_data_normal.csv"
  FULL_TEST_FP = "tests/full_test.csv"

var
  normalData: string
  thresholdPlusData: string
  thresholdMinusData: string

suite "CalcTests":
  setUp:
    normalData = readFile(NORMAL_TEST_FP)
    #thresholdPlusData = readFile(THP_TEST_FP)
    #thresholdMinusData = readFile(THM_TEST_FP)

  test "Test Normal Calcs":
    let results = runCalculationsOnSheet(NORMAL_TEST_FP)
    echo fmt"Checking Short calc coeffecient {results.coeffecient} == 0.520652174"
    check results.coeffecient == 0.520652174

  test "Test Full Calcs":
    let results = runCalculationsOnSheet(FULL_TEST_FP)
    echo fmt"Checking Full calc coeffecient {results.coeffecient} == 0.185"
    check round(results.coeffecient, 3) == 0.185

