## Creates files with all lat/long bounds
import math, strformat, parsecfg, os, strutils

type
  Point* = tuple
    ## A tuple representing lat/long
    lat: float
    long: float

const
  CONFIG_FNAME = "config.ini"

# These are sensible defaults, we override these in config.ini
var
  # Default END/START
  END: Point = (lat: round(-30.811713), long: round(120.944138))
  START: Point = (lat: round(-33.413342), long: round(116.994213))
  INCREMENT = 0.1  # How much precision we want to evaluate


proc points*(startPoint, endPoint: Point): seq[Point] =
  ## Generates points from start long/lat to end long/lat
  result = @[]
  var
    lat: float = startPoint.lat
    long: float =  startPoint.long

  echo fmt"Running from {lat} to {END.lat} and {long} to {END.long}"
  while lat <= endPoint.lat:
    long = startPoint.long
    while long <= endPoint.long:
      let point = (lat: lat, long: long)
      echo "Adding point " & $point
      result.add point
      long = if endPoint.long > startPoint.long: round(long + INCREMENT, 1) else: round(long - INCREMENT, 1)
    lat = if endPoint.lat > startPoint.lat: round(lat + INCREMENT, 1) else: round(lat - INCREMENT, 1)

when isMainModule:
  if not fileExists(CONFIG_FNAME):
    var dict = newConfig()
    dict.setSectionKey("Data", "DataFile", "data.dat")
    dict.setSectionKey("Data", "StartDate", "19700101")
    dict.setSectionKey("Data", "EndDate", "20191231")
    dict.setSectionKey("Data", "DataDelimiter", "|")
    dict.setSectionKey("StartPoint", "lat", $START.lat)
    dict.setSectionKey("StartPoint", "long", $START.long)
    dict.setSectionKey("EndPoint", "lat", $END.lat)
    dict.setSectionKey("EndPoint", "long", $END.long)
    dict.setSectionKey("Data", "increment", $0.1)
    dict.writeConfig(CONFIG_FNAME)

  let config = loadConfig(CONFIG_FNAME)

  # get our config values and override the values initialised at the top of the file
  END = (parseFloat(config.getSectionValue("EndPoint", "lat")), parseFloat(config.getSectionValue("EndPoint", "long")))
  START = (parseFloat(config.getSectionValue("StartPoint", "lat")), parseFloat(config.getSectionValue("StartPoint", "long")))
  INCREMENT = parseFloat(config.getSectionValue("Data", "increment"))

  var
    f = open("data.dat", fmWrite)
    fcounter: int = 0
    counter: int = 0
  for val in points(START, END):
    echo fmt"Writing {val.lat} - {val.long}"
    f.write(fmt"{val.lat},{val.long}|")
    counter.inc
    # Split into files of 500 points
    if counter > 500:
      fcounter.inc
      counter = 0
      f.close()
      f = open(&"data{fcounter}.dat", fmWrite)

  f.close()
