## Main processing logic for the project
## Runs calculations on collated data created by running the data executable
import os, parsecsv, strutils, threadpool, strformat, sugar, sequtils, math, segfaults

# Where to read files from
const
  DATA_DIR = "datasets"

type
  CalculationResults = object
    lat*: float
    long*: float
    coeffecient*: float
    state*: bool

  CalculationResult = object
    netRainFall*: float
    padState*: float
    runOff*: float

# Serializers

proc `$`*(c: CalculationResult): string =
  ## Serialize for CSV
  fmt"{c.netRainfall},{c.padState},{c.runOff}"

proc filename(c: CalculationResults): string =
  ## Serialize for CSV filename
  fmt"{c.lat}-{c.long}.csv"

# Calculation constants
const
  EVAP_FACTOR = 0.7
  RUNOFF_THRESHOLD = -20.0

func calcNetRainfall*(rainfall, evaporation: float): float =
  ## Calc net rainfall by taking evaporation * EVAP_FACTOR from rainfall for this date span
  round(rainfall - (evaporation * EVAP_FACTOR), 2)

func calcPad*(prevPad, netRainfall: float): tuple[padState: float, isRunoff: bool, runoff: float] =
  ## Calculate the pad by summing the pad state from the previous row
  ## with net rainfall so far. If the pad is >= 0.0, we have runoff
  ## and if the pad exceeds the runoff threshold, we return that instead
  result.padState = round(prevPad + netRainfall, 2)
  result.isRunoff = false
  result.runoff = 0.0
  if result.padState > 0.0:
    result.isRunoff = true
    result.runoff = result.padState
    result.padState = 0
  elif result.padState <= RUNOFF_THRESHOLD:
    result.padState = RUNOFF_THRESHOLD

func calcCoeffecient(runoff, totalRainfall: float): float =
  ## The coeffecient is the runoff divided by the total rainfall
  runoff / totalRainfall

proc runCalculationsOnRow*(pad, daily_rain, evap_pan: float): CalculationResult =
  ## Calculate the net rainfall and the pad value for a given date span
  var
    lastEvap: float
    runOff: float

  let netRainfall = calcNetRainfall(
    rainfall=daily_rain,
    evaporation=evap_pan
  )
  let padVal = calcPad(pad, netRainfall)
  if padVal.isRunoff:
    runOff = padVal.runoff

  result.netRainFall = daily_rain
  result.padState = padVal.padState
  result.runOff = runOff

func extractLatLongFromFilename(filename: string): (string, string) =
  let splitName = filename.split("_")[^2..^1]
  (splitName[0], splitName[1].split(".")[0..1].join("."))

proc runCalculationsOnSheet*(sheetPath: string): CalculationResults =
  ## Given a path to a CSV sheet, perform calculations on the data in that sheet
  var
    parser: CSVParser
    (latitude, longitude) = extractLatLongFromFilename(sheetPath)
    pad = RUNOFF_THRESHOLD
    results: seq[CalculationResult] = @[]

  try:
    # Open our sheet
    parser.open(sheetPath)
    parser.readHeaderRow()
    for header in parser.headers:
      if "Silo" in header:
        # If we get here, we've ingested something strange that should have been discarded in the data gather/parse step
        result.state = false
        return result
    # Read rows
    while parser.readRow():

      let cr = runCalculationsOnRow(
        pad=pad,
        daily_rain=parseFloat(parser.rowEntry("daily_rain").strip()),
        evap_pan=parseFloat(parser.rowEntry("evap_pan").strip()),
      )
      pad = cr.padState
      results.add cr

    parser.close()
    # Accumulate all of our rainfall values and runoff values
    let
      netRainfall: float = foldl(results, a + b.netRainfall, 0.0)
      netRunoff: float = foldl(results, a + b.runoff, 0.0)

    # Calculate the coeffecient by dividing net runoff by net rainfall
    let coeffecient = calcCoeffecient(netRunoff, netRainFall)
    result.long = parseFloat(longitude)
    result.lat = parseFloat(latitude)
    result.coeffecient = round(coeffecient, 9)
    if result.coeffecient.classify() == fcNaN:
      result.state = false
    else:
      result.state = true
  except Exception as e:
    echo e.msg
    result.state = false
  finally:
    try:
      parser.close()
    except:
      discard

when isMainModule:
  setMaxPoolSize(4)
  var rows: string = ""
  # Write out headers
  rows.add "lat,long,coeffecient\n"

  # Create a seq to store our thread results (See data.nim for additional info)
  var calcResults = newSeq[FlowVar[CalculationResults]]()
  for kind, path in walkDir(DATA_DIR):
    if kind == pcDir:
      for innerKind, toProcess in walkDir(path):
        calcResults.add(spawn runCalculationsOnSheet($toProcess))

      echo "Syncing"
      sync()
      for val in calcResults:
        # Extract our boxed thread result
        let res = ^val

        if res.state:
          echo &"Writing {res.lat}/{res.long} with ce {res.coeffecient}"
          rows.add fmt"{res.lat},{res.long},{res.coeffecient}" & '\n'

  writeFile("result.csv", rows)
