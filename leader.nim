import parsecfg, osproc, os, strformat

const CONFIG_FNAME = "config.ini"


var count = 0
try:
  for file in walkPattern("data*.dat"):
    echo &"Writing config for {file}"
    var config = loadConfig(CONFIG_FNAME)
    config.setSectionKey("Data", "DataFile", file)
    config.writeConfig(CONFIG_FNAME)
    if dirExists "datasets" / file:
      echo fmt"File {file} exists!"
    else:
      count.inc
      let retval = execCmd("./data")
      echo "Ran with retval: " & $retval
      if retval != 0:
        echo "Error processing latest data file"
        quit(-1)
    if count > 5:
      quit(0)
except:
  quit(1)
