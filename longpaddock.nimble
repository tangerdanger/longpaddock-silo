# Package

version       = "0.1.0"
author        = "Ryan Cotter"
description   = "Rain Coeffecient Calculator"
license       = "MIT"
srcDir        = "src"
bin           = @["longpaddock"]



# Dependencies

requires "nim >= 1.2.6"

task buildBin, "Build the exes":
  exec "nim c -d:release -d:ssl --threads:on data.nim"
  exec "nim c -d:release --threads:on logic.nim"

task winBuild, "Build for windows":
  exec "nim c -d:release --out:getbounds.exe scripts/getbounds"
  exec "nim c --threads:on -d:ssl -d:release --out:data.exe data"
  exec "nim c --threads:on -d:release --out:logic.exe logic"

task test, "Test":
  exec "nim c --threads:on logic.nim"
  exec "nim c -r --threads:on tests/test.nim"
