switch("path", ".")

task build, "Compile everything":
  exec "nim c --threads:on -d:release -d:ssl data.nim"
  exec "nim c -d:release leader.nim"

task getdata, "Get data":
  buildTask()
  exec "./leader"
