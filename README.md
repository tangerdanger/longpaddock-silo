# Installing

## Windows
Install choosenim from the downloads page of choosenim

https://github.com/dom96/choosenim#choosenim

OR

Install Scoop https://scoop.sh/
See
https://nim-lang.org/install_windows.html

## Data format

Populate the data.dat file, which should be a vector of point locations (in lat and long), split by a comma and then the points must be split by a PIPE "|"
For example; 1.2,-104.444|111.1,-0.1| etc

# Building

To build, run `nimble buildBin`


