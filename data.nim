## Retreives data from the longpaddock API
import uri, httpclient, os, strformat, threadpool, parsecsv, streams,
    sugar, strutils, sequtils, parsecfg, segfaults, oids

type
  DataResult* = object
    ## A DataResult stores our parsed data and the associated headers
    headers*: seq[string]
    data*: seq[seq[string]]

  QueryResult* = ref object
    ## A QueryResult is what we return from spawn as a FlowVar
    ## Has to be ref as strings use garbgae collection, hence we can't just be
    ## passing around actual objects
    headers*: string
    data*: string
    state*: bool
    lat*, long*: float


proc get*(d: DataResult, needle: string, default: seq[string] = @[]): seq[string] =
  ## Get a whole column of data
  result = default
  if needle in d.headers:
    result = d.data[d.headers.find(needle)]

proc `$`*(q: QueryResult): string =
  ## String formatter for QueryResult
  fmt"{q.lat} - {q.long}"

# Some handy constants
const
  BASE_URL = parseUri("https://www.longpaddock.qld.gov.au/cgi-bin/silo/DataDrillDataset.php?")
  CSV_DELIM = ","  # The output file's delimiter
  CONFIG_FNAME = "config.ini"  # Where to read config from

# Set some constants
const queryTable = @[
  ("format", "csv"),  # format to get data from endpoint
  ("username", "ryanhcotter@gmail.com"),  # Replace with email
  ("password", "silo"),  # Password can be anything
  ("comment", "RE"),  # Required, but means nothing
]

proc parseData*(parser: var CSVParser): seq[seq[string]] =
  ## Parses response CSV file into a 2d array for parsing
  # For each row in our CSV result, create a 2d array for each header
  result = @[]
  for header in parser.headers:
    # For each header, initialise a new seq[string]
    result.add(newSeq[string]())

  # Insert each cell into the correct column, including headers
  while parser.readRow:
    var idx = 0
    for val in parser.row.items:
      result[idx].add(val)
      idx.inc

iterator getDataResultRows*(data: DataResult): seq[string] =
  ## Iterator that yields parsed data row by row
  echo "Getting data result rows"
  try:
    for outer in 0 ..< min[int](map(data.data, proc(s: seq[string]): int = s.len)):
      var row: seq[string] = @[]
      for inner in data.data:
        row.add(inner[outer])
      yield row
  except IndexDefect:
    echo fmt"Index error when trying to read over {data.data}"
    raise

proc getQueryResultParallel*(targetLatitude, targetLongitude: float, startDate,
    endDate: string): QueryResult {.thread.} =
  ## Threaded function to execute HTTP requests and do light processing in parallel
  new result
  # A ref object needs to be initialized
  result.lat = targetLatitude
  result.long = targetLongitude

  # Make a new HTTP Client to make a request to ask the API for the data
  var client = newHttpClient()

  var curQuery: seq[(string, string)]

  # We want to change the `lon` value in the query parameters for each new value of longitude
  # To do this, we go through our base queryTable seq and add all values from it to our curQuery variable
  for attr in queryTable:
    curQuery.add(attr)

  # Add our variable values to the query
  curQuery.add(("lon", $targetLongitude))
  curQuery.add(("lat", $targetLatitude))
  curQuery.add(("start", startDate))
  curQuery.add(("finish", endDate))

  # encodeQuery turns our list of query parameters to a querystring format (ie ?format=csv&lon=X.Y)
  let finalUrl = $BASE_URL & "?" & decodeURL(encodeQuery(curQuery))

  var
    reader: CSVParser
    retryCounter: int = 5
    lastData = ""

  echo &"Getting data for {finalUrl}"
  while retryCounter > 0:
    try:
      # Get the data
      let resp = client.get(finalUrl)

      # Stream the data to mimic opening a CSV file for CSVParser
      var dataStream = newStringStream(resp.body)

      let data = dataStream.readAll()
      if "request rejected" in data.toLowerAscii() or "silo is unable to supply" in data.toLowerAscii():
        result.state = false
        result.headers = ""
        echo "Request was rejected, perhaps we're hitting the API too fast?"
        return result

      # Reset stream position indicator
      dataStream.setPosition(0)

      # Open a temporary file. By using an OID, we're trying our best to not encounter a filename collision
      # The variant of open we're using dumps the StringStream into the file
      reader.open(dataStream, &"tmp{$genOid()}.csv")

      # We're done, so close the stream so we don't need to keep it in memory
      dataStream.close()

      # Read our header rows into the parser
      reader.readHeaderRow()

      # Our headers for our output file
      result.headers = reader.headers.join(CSV_DELIM) & "\n"

      echo "Parsing data"
      # Parse our data
      let
        parsedData = reader.parseData
        lastData = parsedData.join("\n")
        dresult = DataResult(headers: reader.headers, data: parsedData)

      # We're got our data from the file, close it ASAP so we don't hof file descriptors
      reader.close()

      # Serialize our data to a string
      result.data = ""
      for row in dresult.getDataResultRows:
        result.data.add(row.join(CSV_DELIM) & "\n")

      # Success
      result.state = true
      echo "Returning state!"

      # Tidy up
      return result
    except IndexError as e:
      echo fmt"Index error in get data: {e.msg}: Exiting"
      echo lastData
      quit(-1)
    except Exception as e:
      # We've hit an error, print something useful and set the task's state to failed
      retryCounter.dec
      echo &"Error in get data: {e.msg}"

  # Failed to retry the API, set as failed
  result.headers = ""
  result.data = &"{$targetLongitude}|{$targetLongitude}"
  result.state = false
  reader.close()

# Entry point if running as a script, like if __name__ == '__main__' or int main(argc, argv) {}
when isMainModule:

  # Set max number of threads in threadpool. Adjust as necessary, depending on how many threads your machine can happily run.
  # Might need to run it a bit and tweak it
  setMaxPoolSize(4)

  # If our config doesn't exist, create it
  if not fileExists(CONFIG_FNAME):
    var dict = newConfig()
    dict.setSectionKey("Data", "DataFile", "data.dat")
    dict.setSectionKey("Data", "StartDate", "19700101")
    dict.setSectionKey("Data", "EndDate", "20191231")
    dict.setSectionKey("Data", "DataDelimiter", "|")
    dict.setSectionKey("StartPoint", "lat", "")
    dict.setSectionKey("StartPoint", "long", "")
    dict.setSectionKey("EndPoint", "lat", "")
    dict.setSectionKey("EndPoint", "long", "")
    dict.setSectionKey("Data", "increment", $0.1)
    dict.writeConfig(CONFIG_FNAME)

  let config = loadConfig(CONFIG_FNAME)

  # Clean out datasets from our previous run/s
  # Comment this out if you want to keep data inbetween runs, but cautious of
  for fname in walkDir("datasets", checkDir = true):
    if dirExists(fname.path):
      removeDir(fname.path)

  # Read our config values
  var
    delimiter = config.getSectionValue("Data", "DataDelimiter")
    startDate = config.getSectionValue("Data", "StartDate")
    endDate = config.getSectionValue("Data", "EndDate")
    file = config.getSectionValue("Data", "DataFile")  # Comment out if running on all .dat files

  # If we want to blind iterator over our .dat files, uncomment this and
  # indent the code beneath
  #for file in walkPattern("data*.dat"):

  # Our queryResults is a seq of FlowVar (result boxes for thread returns)
  var queryResults = newSeq[FlowVar[QueryResult]]() # For storing thread results

  # Try to open our data file
  var dataFile = block:
    try:
      echo &"Opening {file}"
      open(&"{file}")
    except IOError:
      # Probably the .dat file specified in config.ini doesn't exist, try running the `getbounds` script
      echo &"No data file named '{file}' located in this folder, exiting"
      quit()

  # Iterate over our data file, which contains pipe (|) delimited tuples of lat and long coordinates (comma delimited)
  # Delimiter is specified in the config.ini file, so can be changed as necessary
  for point in dataFile.readAll() # Read the whole .dat file
    .split(delimiter)  # Split by delimiter between lat/long points
    .filter(p => not isEmptyOrWhitespace(p)).map(d => d.split(",")):  # Filter out empty cells and then split each cell into a tuple of lat/long
    try:
      # Spawn a thread to execute the query in parallel
      let res = spawn getQueryResultParallel(parseFloat(point[0].strip()), parseFloat(point[1].strip()), startDate, endDate)
      queryResults.add(res)
    except Exception as e:
      echo e.msg
      continue

  datafile.close()
  try:
    echo &"Finished gathering results for {file}"
    var sucCounter = 0
    # Sync awaits all outstanding threads to finish
    sync()
    echo "Finished syncing"
    for val in queryResults:
      sleep(10)
      # Yield the value of the query
      # This forces the program to wait until the query has completed
      try:
        # The ^ operator blocks until the thread has returned and returns the contained type inside the FlowVar
        let res = ^val

        # Ignore if failed
        if res.state:
          # Because we've split our CSV result from the API into
          # a header/body, we can just write these to a string as they are, saving our main thread from doing any heavy lifting
          var dataToWrite = ""

          # Write our CSV data
          dataToWrite.add(res.headers)
          dataToWrite.add(res.data)

          # Make sure our parent directories exist
          let parentPath = "datasets"
          discard existsOrCreateDir(parentPath)
          discard existsOrCreateDir(parentPath / file)
          let folderPath = parentPath / file
          let targetPath = fmt"silo_{res.lat}-{res.long}.csv".replace("-", "_")
          echo &"Writing results to {folderPath / targetPath}"

          try:
            writeFile(folderPath / targetPath, dataToWrite)
            # Commit our data to file
            #moveFile(targetPath, parentPath / targetPath)
            sucCounter.inc
          except Exception as e:
            echo &"Exception occurred: {e.msg}"
            sleep(10)
        else:
          echo &"WARNING!: Results for Long/Lat {res.data} failed!"
      except NilAccessDefect as e:
        # This crops up when we can't open a file for reading/writing.
        # Usually this means we've hit the file descriptor limit or
        # our OS can't spawn a thread to open a new file
        echo &"ERROR: {e.msg}"
        continue
      echo &"Finished, wrote {sucCounter} results to file"
  except Exception as oe:
    echo &"Cleaning up due to general failue: {oe.msg}"
    let parentPath = "datasets"
    let folderPath = parentPath / file
    removeDir(folderPath)
    quit(-1)
    # Uncomment to enable archiving
    #if sucCounter > 0:
    #  moveFile(config.getSectionValue("Data", "DataFile"), "archive/" & config.getSectionValue("Data", "DataFile"))
    # Once we're all done reading, close our file
